<?php
    include_once __DIR__.'/database.php';

    // SE CREA EL ARREGLO QUE SE VA A DEVOLVER EN FORMA DE JSON
    $data = array();
    // SE VERIFICA HABER RECIBIDO EL ID
    if( isset($_POST['id']) ) {
        $dato = $_POST['id'];
        
        if ( $result = $conexion->query("SELECT * FROM productos WHERE id = '{$dato}'")) {
			$row = $result->fetch_array(MYSQLI_ASSOC);
            

            if(!is_null($row)) {
                foreach($row as $key => $value) {
                    $data[$key] = utf8_encode($value);
                }
            }
			$result->free();
		} else {
            die('Query Error: '.mysqli_error($conexion));
        }

        if ( $result = $conexion->query("SELECT * FROM productos WHERE nombre = '{$dato}'")) {
			$row = $result->fetch_array(MYSQLI_ASSOC);

            if(!is_null($row)) {
                foreach($row as $key => $value) {
                    $data[$key] = utf8_encode($value);
                }
            }
			$result->free();
		} else {
            die('Query Error: '.mysqli_error($conexion));
        }

        if ( $result = $conexion->query("SELECT * FROM productos WHERE marca = '{$dato}'")) {
			$row = $result->fetch_array(MYSQLI_ASSOC);

            if(!is_null($row)) {
                foreach($row as $key => $value) {
                    $data[$key] = utf8_encode($value);
                }
            }
			$result->free();
		} else {
            die('Query Error: '.mysqli_error($conexion));
        }

        if ( $result = $conexion->query("SELECT * FROM productos WHERE detalles = '{$dato}'")) {
			$row = $result->fetch_array(MYSQLI_ASSOC);

            if(!is_null($row)) {
                foreach($row as $key => $value) {      
                    $data[$key] = utf8_encode($value);
                }
            }
			$result->free();
		} else {
            die('Query Error: '.mysqli_error($conexion));
        }

		$conexion->close();
    } 

    
    
    // SE HACE LA CONVERSIÓN DE ARRAY A JSON
    echo json_encode($data, JSON_PRETTY_PRINT);
?>