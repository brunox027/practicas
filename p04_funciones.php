<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php

    //1
    function divisible(){
        $numero = $_GET['numero'];
        if ($numero%5==0&&$numero%7==0){
            echo $numero. ' es divisible entre 5 y 7';
        }
        else{
            if ($numero%5==0||$numero%7==0){
                if ($numero%5==0) {
                    echo $numero. ' es divisible entre 5';
                    echo '<br>';
                }
                if ($numero%7==0) {
                    echo $numero. ' es divisible entre 7';
                    echo '<br>';
                }
            }
            else{
                echo $numero. ' no es divisible entre 5 o 7';
            }
        }
    }
    divisible();

    //2
    function aleadiv(){
        $paroimpar = false;
        $conti = 0;
        do{
            $alea = array(0 => $n=rand(100, 999), 1 => $nu=rand(100, 999), 2 => $num=rand(100, 999));
            foreach($alea as $value){
                echo $value. ', ';
            }
            echo '<br>';
            if($n%2==1 && $nu%2==0 && $num%2==1){
                $paroimpar = true;
            }
            $conti ++;
        } while ($paroimpar == false);
        $contn = $conti * 3;
        echo '<br>';
        echo 'Hay '.$contn. ' numeros y ' .$conti. ' iteraciones';
    }
    aleadiv();

    //3
    function diviran(){
        print "Version WHILE";
        echo '<br>';
        $numero = $_GET['numero'];
        $n = rand(1, 100);
        while ($numero%$n!=0){
            $n = rand(1, 100);
            echo $numero. ' no es divisible entre ' .$n;
            echo '<br> Numero random ';
            echo $n. '<br>';
        }
        echo $numero. ' es divisible entre ' .$n;
        echo '<br><br>';
    
        print "Version Do-While";
        echo '<br>';
        $numero = $_GET['numero'];
        $no = rand(1, 100);
        do {
            $no = rand(1, 100);
            echo $numero. ' no es divisible entre ' .$no;
            echo '<br> Numero random ';
            echo $no. '<br>';
        } while($numero%$no!=0);
        echo $numero. ' es divisible entre ' .$no;
    }
    diviran();

    //4
    function charint(){
        for($i = 97; $i < 123; $i ++){
            $arreglo[$i] = chr($i);
        }
        echo '<table>
                    <tr>
                        <td><strong>Letras</strong></td>';
        foreach($arreglo as $clave => $elemento){
            echo '<td>'.$elemento.'</td>';
        }
        
        echo '</tr>
            <tr>
                <td><strong>Indices</strong></td>';
        foreach($arreglo as $clave => $elemento){
            echo '<td>'.$clave.'</td>';
        }
        echo '</tr>';
    }
    charint();

    ?>
</html>