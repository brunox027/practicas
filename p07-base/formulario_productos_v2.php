<!DOCTYPE html >
<html>

  <head>
    <meta charset="utf-8" >
    <title>Registro de productos</title>
    <style type="text/css">
      ol, ul { 
      list-style-type: none;
      }
    </style>
  </head>
  <?php
    /* MySQL Conexion*/
    $link = mysqli_connect("localhost", "root", "root", "demoDB");
    // Chequea coneccion
    if($link === false){
      die("ERROR: No pudo conectarse con la DB. " . mysqli_connect_error());
    }
    // Ejecuta la actualizacion del registro
    $sql = "UPDATE personas SET email='josem_nuevo@mail.com' WHERE id=3";
    if(mysqli_query($link, $sql)){
    echo "Registro actualizado.";
    } else {
    echo "ERROR: No se ejecuto $sql. " . mysqli_error($link);
  }
  // Cierra la conexion
  mysqli_close($link);
?>

  <body>
    <h1>Registro de productos</h1>

    <form id="formularioTenis" action="http://localhost/p07-base/get_productos_xhtml_v2.php" method="post">

    <h2>Nombre del producto</h2>
        <label for="form-name">Nombre:</label> <input type="text" name="name" id="form-name">
      
    <h2>Marca del producto</h2>
        <label for="form-name">Marca:</label> <input type="text" name="marca" id="form-name">
        
    <h2>Modelo del producto</h2>
        <label for="form-name">Modelo:</label> <input type="text" name="modelo" id="form-name">

    <h2>Precio del producto</h2>
        <label for="form-name">Precio:</label> <input type="number" name="precio" id="form-name">

    <h2>Descripcion del producto</h2>
        <label for="form-story">Descripcion:</label><br><textarea name="story" rows="4" cols="60" id="form-story" placeholder="Pero cortito"></textarea></li>

    <h2>Unidades disponibles del producto</h2>
        <label for="form-name">Unidades:</label> <input type="number" name="number" id="form-name">

      <p>
        <input type="submit" value="Subir">
        <input type="reset">
      </p>

    </form>
  </body>
</html>