<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
    <?php
	    if(isset($_GET['tope']))
        {
		    $tope = $_GET['tope'];
        }
        else
        {
            die('Parámetro "tope" no detectado...');
        }

	    if (!empty($tope))
	    {
		    /** SE CREA EL OBJETO DE CONEXION */
		    @$link = new mysqli('localhost', 'root', '12345678a', 'marketzone');
            /** NOTA: con @ se suprime el Warning para gestionar el error por medio de código */

		    /** comprobar la conexión */
		    if ($link->connect_errno) 
		    {
			    die('Falló la conexión: '.$link->connect_error.'<br/>');
			    //exit();
		    }

		    /** Crear una tabla que no devuelve un conjunto de resultados */
		    if ( $result = $link->query("SELECT * FROM productos WHERE unidades <= $tope") ) 
		    {
                /** Se extraen las tuplas obtenidas de la consulta */
			    $row = $result->fetch_all(MYSQLI_ASSOC);
				//$unit = $link->query("SELECT * FROM productos WHERE unidades = '{$unidades}'");
				
                /** Se crea un arreglo con la estructura deseada */
                foreach($row as $num => $registro) {            // Se recorren tuplas
                    foreach($registro as $key => $value) {      // Se recorren campos
                        $data[$num][$key] = utf8_encode($value);
                    }
                }

			    /** útil para liberar memoria asociada a un resultado con demasiada información */
			    $result->free();
		    }
			

            /** Se devuelven los datos en formato JSON */
            echo json_encode($data, JSON_PRETTY_PRINT);
			echo '<br></br>';

			$link->close();
	    }
	?>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Producto</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"/>
	</head>
	<body>
		<h3>PRODUCTO</h3>

		<br>
		
		<?php
		
            echo '<table class="table">
			<thead class="thead-dark">
				<tr>
				<th scope="col">#</th>
				<th scope="col">Nombre</th>
				<th scope="col">Marca</th>
				<th scope="col">Modelo</th>
				<th scope="col">Precio</th>
				<th scope="col">Unidades</th>
				<th scope="col">Detalles</th>
				<th scope="col">Imagen</th>
				</tr>
			</thead>';
			if( isset($row) ) : 
				do{
				echo '<tbody>
					<tr>
						<th scope="row">'. $row["id"] .'</th>';
						echo'<td>'. $row["nombre"] .'</td>';
						echo'<td>'. $row["marca"] .'</td>';
						echo'<td>'. $row["modelo"] .'</td>';
						echo'<td>'. $row["precio"] .'</td>';
						echo'<td>'. $row["unidades"] .'</td>';
						echo'<td>'. utf8_encode($row["detalles"]) .'</td>';
						echo'<td><img src=""'. $row["imagen"]. 'alt="producto"/></td>';
					echo '</tr>
				</tbody>
			</table>';
            }while($tope >= $result);
        ?>
		<?php elseif(!empty($id)) : ?>
			<script>
                alert('El ID del producto no existe');
            </script>
		<?php endif; ?>
		<p>
    		<a href="http://validator.w3.org/check?uri=referer"><img src="http://www.w3.org/Icons/valid-xhtml11" alt="Valid XHTML 1.1" height="31" width="88" /></a>
  		</p>
	</body>
</html>